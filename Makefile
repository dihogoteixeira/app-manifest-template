.PHONY: help generate-manifest deploy-manifest
.DEFAULT: help

# KUBERNETES VARIEBLAS TO DEPLOYMENT
K8S_DIR		?= $(CI_BUILDS_DIR)/infra/eks
K8S_BUILD_DIR ?= $(CI_BUILDS_DIR)/infra/eks/generate_manifest
K8S_FILES   := $(shell find $(K8S_DIR) -name '*.yml' -o -name '*.yaml' | sed 's:$(K8S_DIR)/::g')

help:
	@echo "make generate-manifest"
	@echo "		Subistituir variaveis nos manifestos do deployment"
	@echo "make deploy-manifest"
	@echo "		Aplicar os manifestos no ambiente"


generate-manifest:
	for file in $(K8S_FILES); do \
	  mkdir -p `dirname "$(K8S_BUILD_DIR)/$$file"` ; \
	  envsubset <$(K8S_DIR)/$$file | tee -a $(K8S_BUILD_DIR)/$$file; \
	done

deploy-manifest:
	kubectl apply -f $(K8S_BUILD_DIR) --record
	kubectl get all
